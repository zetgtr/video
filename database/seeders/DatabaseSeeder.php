<?php

namespace Video\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            VideoSettings::class,
            MenuSeeder::class
        ]);
    }
}