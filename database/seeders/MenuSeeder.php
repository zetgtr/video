<?php

namespace Video\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>989,'name'=>'Видео','position'=>'left','logo'=>'fal fa-video','controller'=>'Video\VideoController','url'=>'video','parent'=>5, 'order'=>1],
        ];
    }
}
