<?php

namespace Video\Seeders;

use Illuminate\Database\Seeder;

class VideoSettings extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('video_settings')->insert($this->getData());
    }

    public function getData(): array
    {
        return [
            ['id'=>1,'url'=>'video'],
        ];
    }
}
