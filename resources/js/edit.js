class Edit {
    constructor() {
        this.items = document.querySelectorAll(".button-edit"),
            this.form = document.querySelector("#form_event"),
            this.method = document.querySelector('input[type="hidden"][name="_method"]'),
            this.title = document.querySelector("#title"),
            this.description = document.querySelector("#description"),
            this.video = document.querySelector("#video"),
            this.header = document.querySelector("h4"),
            this.buttonClose = document.querySelector(".button_close"),
            this.addEvent()
    }
    addEvent() {
        const context = this;
        this.items.forEach(el=>el.addEventListener("click", e=>{
                e.preventDefault(),
                    context.editEvent(el)
            }
        )),
            this.buttonClose.addEventListener("click", e=>{
                    e.preventDefault(),
                        this.buttonClose.classList.add("d-none"),
                        this.title.value = "",
                        this.description.innerText = "",
                        this.video.innerText = "",
                        this.form.action = "/admin/video",
                        document.querySelectorAll("#category_id option").forEach((el) => {
                            el.selected = false;
                        });
                        this.method.value = "POST",
                        this.header.innerText = "Добавить видео"
                }
            )
    }
    editEvent(el) {
        axios.get(el.href).then(({data})=>{
                this.buttonClose.classList.remove("d-none"),
                    this.title.value = data.title,
                    document.querySelectorAll("#category_id option").forEach((el) => {
                        el.selected = el.value == data.category_id;
                    });
                    this.description.innerText = data.description,
                    this.video.innerText = data.video,
                    this.form.action = "/admin/video/" + data.id,
                    this.method.value = "PUT",
                    this.header.innerText = "Редактировать видео"
            }
        )
    }
}
$(document).ready(()=>{
        new Edit()
    }
);
