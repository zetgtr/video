@extends('layouts.admin')
@section('title',"Видео")
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <x-admin.navigation :links="$links" />
                <div class="card-body">
                    <x-warning />
                    <div class="row row-page-create">
                        <div class="col-md-8 order-md-first">
                            <div class="dd nestable" id="nestable" data-max="1">
                                <x-video::admin.category.categories :categories="$categories"/>
                            </div>
                        </div>
                        <div class="col-md-4 order-md-first ">
                            <x-video::admin.category.form />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="route_dd" value="{{  route('admin.video_category.order') }}">
    <script src="{{asset('assets/js/admin/dnd.js')}}" ></script>
    @vite('resources/js/video/category/edit.js')
    <script src="{{ asset('assets/js/admin/delete.js') }}"></script>
    <script src="{{ asset('assets/js/admin/show.js') }}"></script>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.video.index")}}">Видео</a></li>
            <li class="breadcrumb-item active" aria-current="page">Категории</li>
        </ol>
    </div>
@endsection
