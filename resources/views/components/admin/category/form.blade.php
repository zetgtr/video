<form action="{{ route('admin.video_category.store') }}" id="form_event" method="POST">
    @csrf
    @method('POST')
    <h4>Добавить категорию</h4>
    <div class="form-group">
        <label>Заголовок</label>
        <input type="text" id="title" class="form-control" name="title">
    </div>
    <div class="form-group">
        <label>Ссылка</label>
        <input type="text" id="url_video" class="form-control" name="url">
    </div>
    <div class="form-group">
        <label>Описание</label>
        <textarea class="form-control" id="description" name="description"></textarea>
    </div>

    <input type="submit" value="Сохранить" class="btn btn-sm btn-success button_save">
    <input type="submit" value="Отменить" class="btn btn-sm btn-danger d-none button_close">
</form>


