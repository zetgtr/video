@forelse($videos as $video)
    <li class="list-group-item delete-element" data-id="2" data-name="Файлы" data-position="left">
        <div class="">
            <span>{{ $video->title }}
            @if($video->category)
                - {{ $video->category->title }}
            @endif
            </span>
        </div>
        <div class="dnd-edit" style="top: 0">
            <a href="{{ route('admin.video.destroy',$video) }}"  style="color: #fff" class="button-delete delete btn btn-danger btn-xs pull-right" data-owner-id="1">
                <i class="fa fa-times" aria-hidden="true"></i>
            </a>
            <a href="{{ route('admin.video.edit',$video) }}" style="color: #fff" class="button-edit btn btn-success btn-xs pull-right" data-owner-id="1">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </a>
        </div>
    </li>
@empty
    <div class="d-flex justify-content-center align-items-center h-100">
        <p>Не создано</p>
    </div>
@endforelse
<br>
{!! $videos->onEachSide(5)->links() !!}
