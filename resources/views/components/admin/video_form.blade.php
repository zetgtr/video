@php
    $categories = \Video\Models\VideoCategory::orderBy('order')->get();
@endphp
<form action="{{ route('admin.video.store') }}" id="form_event" method="POST">
    @csrf
    @method('POST')
    <h4>Добавить видео</h4>
    <div class="form-group">
        <label>Категория</label>
        <select name="category_id" id="category_id" class="form-select">
            <option value="0">-- Выбирите --</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->title }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Заголовок</label>
        <input type="text" id="title" class="form-control" name="title">
    </div>
    <div class="form-group">
        <label>Описание</label>
        <textarea class="form-control" id="description" name="description"></textarea>
    </div>
    <div class="form-group">
        <label>Код видео</label>
        <textarea class="form-control" id="video" name="video"></textarea>
    </div>
    <input type="submit" value="Сохранить" class="btn btn-sm btn-success button_save">
    <input type="submit" value="Отменить" class="btn btn-sm btn-danger d-none button_close">
</form>


