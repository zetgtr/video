@extends('layouts.admin')
@section('title',"Статьи")
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <x-admin.navigation :links="$links" />
                <x-articles::category />
            </div>
        </div>
    </div>
@endsection
