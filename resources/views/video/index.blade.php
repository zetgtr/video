@extends('layouts.admin')
@section('title',"Видео")
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <x-admin.navigation :links="$links" />
                <div class="card-body">
                    <x-warning />
                    <div class="row">
                        <ul class="list-group col-lg-8">
                            <x-video::admin.video />
                        </ul>
                        <div class="col-lg-4">
                            <x-video::admin.video_form />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @vite('resources/js/video/edit.js')
    <script src="{{ asset('assets/js/admin/delete.js') }}"></script>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Видео</li>
        </ol>
    </div>
@endsection