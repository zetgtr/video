@extends('layouts.admin')
@section('title',"Видео")
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <x-admin.navigation :links="$links" />
                <x-video::settings />
            </div>
        </div>
    </div>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.video.index")}}">Видео</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройки</li>
        </ol>
    </div>
@endsection