<?php

use Illuminate\Support\Facades\Route;
use Video\Http\Controllers\VideoController;
use Video\Http\Controllers\CategoryController;


Route::middleware('web')->group(function (){

    Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
        Route::resource('video', VideoController::class);
        Route::resource('video_category', CategoryController::class);
        Route::get('settings/video', [VideoController::class,'settings'])->name('video.settings');
        Route::get('category/video/publish/{video_category}', [CategoryController::class,'publish'])->name('video_category.publish');
        Route::post('category/video/order', [CategoryController::class,'order'])->name('video_category.order');
        Route::post('settings/video/{settings}', [VideoController::class,'settingsUpdate'])->name('video.settings.update');
    });
    try {
         $settings = \Video\Models\Settings::find(1);
         Route::get('/'.$settings->url,[VideoController::class,'frontIndex'])->name('video');
         Route::get('/'.$settings->url . '/{url}',[CategoryController::class,'show'])->name('video.category');
    } catch (Exception $exception) {
        //throw $th;
    }
});
