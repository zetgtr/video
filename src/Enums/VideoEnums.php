<?php

namespace Video\Enums;

enum VideoEnums: string
{
    case VIDEO = "video";
    case CATEGORY = "category";
    case SETTINGS = "settings";
}

