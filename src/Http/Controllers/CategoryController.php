<?php

namespace Video\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Video\Enums\VideoEnums;
use Video\Http\Request\CategoryRequest;
use Video\Models\VideoCategory;
use Video\QueryBuilder\VideoBuilder;

class CategoryController extends Controller
{
    public function index(VideoBuilder $builder)
    {
        return view('video::category.index',[
            'links' => $builder->getLinks(VideoEnums::CATEGORY->value),
            'categories' => VideoCategory::orderBy('order')->get()
        ]);
    }
    public function store(CategoryRequest $request)
    {
        VideoCategory::create($request->validated());
        return redirect()->back()->with('success','Успешно добавлено');
    }
    public function update(CategoryRequest $request,VideoCategory $videoCategory)
    {
        $videoCategory->fill($request->validated());
        $videoCategory->save();
        return redirect()->back()->with('success','Успешно обновлено');
    }

    public function order(Request $request, VideoBuilder $builder){
        $builder->setOrderCategory($request->all()['items']);
    }


    public function edit(VideoCategory $videoCategory)
    {
        return $videoCategory;
    }

    public function publish(VideoCategory $videoCategory)
    {
        $videoCategory->publish = !$videoCategory->publish;
        $videoCategory->save();
        return ['status'=>true,'publish'=>$videoCategory->publish];
    }

    public function show(string $url){
        $videoCategory = VideoCategory::with('videos')->where('url',$url)->first();
        return view('video::video.front.show',[
            'category'=>$videoCategory,
            'breadcrumbs'=>array_merge(config('video.routes'),[["title"=>$videoCategory->title]]),
            'categories' => VideoCategory::orderBy('order')->get()
        ]);
    }

    public function destroy(VideoCategory $videoCategory)
    {
        try {
            $videoCategory->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.category.destroy.success')];
        } catch (\Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.category.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }

}
