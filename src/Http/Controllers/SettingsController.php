<?php

namespace Articles\Http\Controllers;


use Illuminate\Http\Request;
use Articles\Enums\ArticlesEnums;
use Articles\Models\Settings;
use Articles\QueryBuilder\ArticlesBuilder;
use Articles\Requests\UpdateSettingsRequest;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ArticlesBuilder $articlesBuilder)
    {
        return view('articles::articles.settings',[
            'links' => $articlesBuilder->getLinks(ArticlesEnums::SETTINGS->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSettingsRequest $request, Settings $settings)
    {
        $updated = Settings::query()->find($request->validated('id'));
        $updated = $updated->fill($request->validated());
        if ($updated->save()) {
            return \redirect()->route('admin.articles.settings.create')->with('success', __('messages.admin.articles.settings.success'));
        }

        return \back()->with('error', __('messages.admin.articles.settings.fail'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
