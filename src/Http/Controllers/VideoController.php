<?php

namespace Video\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use Illuminate\Contracts\Validation\Factory;
use Video\Enums\VideoEnums;
use Video\Http\Request\CreateVideoRequest;
use Video\Http\Request\UpdateSettingsRequest;
use Video\Models\Settings;
use Video\Models\Video;
use Video\Models\VideoCategory;
use Video\QueryBuilder\VideoBuilder;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(VideoBuilder $builder)
    {
        return view('video::video.index',[
            'links' => $builder->getLinks(VideoEnums::VIDEO->value)
        ]);
    }


    public function settings(VideoBuilder $builder)
    {
        return view('video::video.settings',[
            'links' => $builder->getLinks(VideoEnums::SETTINGS->value)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ArticlesBuilder $articlesBuilder,Factory $factory)
    {

        return view('articles::articles.create',[
            'linksContent' => $articlesBuilder->getLinksContent(ArticlesEnums::CONTENT->value),
            'links' => $articlesBuilder->getLinks(ArticlesEnums::POST->value),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateVideoRequest $request): RedirectResponse
    {
        $video = Video::create($request->validated());
        if ($video) {
            return \redirect()->back()->with('success', 'Видео успешно добавлено');
        }

        return \redirect()->back()->with('error', "Ошибка добавления видео");
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function settingsUpdate(UpdateSettingsRequest $request, Settings $settings): RedirectResponse
    {
        $settings = $settings->fill($request->validated());
        if ($settings->save()) {
            return \redirect()->back()->with('success', 'Настройки успешно обновлены');
        }

        return \redirect()->back()->with('error', "Ошибка обновления настроик");
    }

    /**
     * Display the specified resource.
     */
    public function show(Video $video)
    {
        return $video;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Video $video)
    {
        return $video;
    }

    /**
     * Handle the incoming request.
     */
    public function frontIndex()
    {
        return \view('video::video.front.index',[
            'videos'=>Video::orderBy('created_at', 'desc')->paginate(Settings::first()->paginate),
            'categories' => VideoCategory::orderBy('order')->get(),
            'settings'=> Settings::query()->find(1),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CreateVideoRequest $request, Video $video)
    {
        $video = $video->fill($request->validated());
        if ($video->save()) {
            return \redirect()->back()->with('success', "Успешно обновлено");
        }

        return \back()->with('error', __('messages.admin.articles.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Video $video)
    {
        try {
            $video->delete();
            $response = ['status' => true,'message' => __('messages.admin.articles.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.articles.destroy.fail') . $exception->getMessage()];
        }

        return $response;
    }
}
