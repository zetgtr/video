<?php

namespace Video\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'url' => 'required|string',
            'description' => 'nullable|string'
        ];
    }

    public function prepareForValidation()
    {
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug($this->title)
            ]);
        }
    }

        public function authorize(): bool
    {
        return \Auth::user()->is_admin;
    }
}
