<?php

namespace Video\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class CreateVideoRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required'],
            'description' => ['nullable'],
            'duration' => ['nullable'],
            'video' => ['required'],
            'img' => ['nullable'],
            'category_id' => ['nullable'],
        ];
    }

    protected function prepareForValidation()
    {
        $images = null;
        if ($this->file('images')) {
            $images = [];
            $image = $this->file('images');
            $file = $image;
            $image = new \Imagick($file->getRealPath());
            $image->setImageFormat('webp');


            $fileName = "/".uniqid() . '.webp';
            $folderName = 'video/images';
            $disk = Storage::disk('public');
            if (!$disk->exists($folderName)) {
                $disk->makeDirectory($folderName);
            }

            $image->writeImage( $disk->path($folderName) . $fileName);
            $images = "/storage/".$folderName.$fileName;

        }

        $this->merge([
            'img' => $images,
            'category_id' => $this->category_id ? $this->category_id : null
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
