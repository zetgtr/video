<?php

namespace Video\Models;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    protected $fillable = [
        'title',
        'description',
        'url',
        'publish',
        'order',
    ];

    public function videos(){
        return $this->HasMany(Video::class,'category_id','id');
    }
}
