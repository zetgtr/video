<?php

namespace Video\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\QueryBuilder\RolesBuilder;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Video\Views\Admin\Settings;
use Video\Views\Admin\Video;


class VideoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        }

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
        ], 'migrations');
        $this->publishes([
            __DIR__.'/../../config/video.php' => config_path('video.php'),
        ], 'config_video');
        $this->publishes([
            __DIR__.'/../../resources/js' => resource_path('js/video'),
        ], 'video_script');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/video'),
        ], 'video_views');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'video');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'video');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(Video::class, 'video::admin.video');
        Blade::component(Settings::class, 'video::settings');
    }

    private function singletons()
    {
//        $this->app->singleton(Articles::class, function ($app) {
//            $articlesBuilder = $app->make(ArticlesBuilder::class);
//            return new Articles($articlesBuilder);
//        });
//        $this->app->singleton(EditContent::class, function ($app, $parameters) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            $rolesBuilder = $app->make(RolesBuilder::class);
//            $articleItem = $parameters['articleItem'];
//
//            return new EditContent($categoryBuilder, $articleItem, $rolesBuilder);
//        });
//        $this->app->singleton(Content::class, function ($app) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            $rolesBuilder = $app->make(RolesBuilder::class);
//            return new Content($categoryBuilder,$rolesBuilder);
//        });
//        $this->app->singleton(Category::class, function ($app) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            return new Category($categoryBuilder);
//        });
//        $this->app->singleton(Settings::class, function ($app) {
//            return new Settings();
//        });
    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(QueryBuilder::class, ArticlesBuilder::class);
//        $this->app->bind(QueryBuilder::class, CategoryBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/video.php', 'video');
        $this->singletons();
    }
}
