<?php

namespace Video\QueryBuilder;

use Illuminate\Database\Eloquent\Collection;
use Video\Enums\VideoEnums;
use Video\Models\VideoCategory;

class VideoBuilder
{
    public function getLinks($key)
    {
        $links = [
            VideoEnums::VIDEO->value => ['url'=> route('admin.video.index'), 'name' => 'Видео'],
            VideoEnums::CATEGORY->value => ['url'=> route('admin.video_category.index'), 'name' => 'Категории'],
            VideoEnums::SETTINGS->value => ['url'=> route('admin.video.settings'),  'name' => 'Настройки']
        ];

        if($key) $links[$key]['active'] = true;

        return $links;
    }

    public function getAll(): Collection
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function setOrderCategory(array $items)
    {
        foreach ($items as $key=>$item)
        {
            $category = VideoCategory::query();
            $category->where('id',$item['id'])->update(['order'=>$key]);
        }
    }

}
