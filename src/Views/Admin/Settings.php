<?php

namespace Video\Views\Admin;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Settings extends Component
{
    public function __construct()
    {
        $this->settings = \Video\Models\Settings::query()->find(1);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('video::components.admin.settings',['settings'=>$this->settings]);
    }
}
