<?php

namespace Video\Views\Admin;

use Illuminate\View\Component;

class Video extends Component
{
    public function __construct()
    {
        try {
            \Video\Models\Video::orderBy('created_at','DESC')->get();
        }catch (\Throwable $exception) {
            dd($exception->getMessage());
        }
        $this->videos = \Video\Models\Video::orderBy('created_at','DESC')->paginate(20);
    }

    public function render(): string
    {
        return view('video::components.admin.video',['videos'=>$this->videos]);
    }
}
